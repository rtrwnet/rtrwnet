<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Landing Page</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: RT RW Net
  * Updated: May 30 2023 with Bootstrap v5.3.0
  * Template URL: https://bootstrapmade.com/free-bootstrap-landing-page/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container-fluid">

      <div class="logo">
        {{-- <h1><a href="index.html">RT RW Net</a></h1> --}}
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <button type="button" class="nav-toggle"><i class="bx bx-menu"></i></button>
      <nav class="nav-menu">
        <ul>
          @role('User')
            <li><a href="{{ url('/view-order') }}" class="scrollto">Beli Voucher</a></li>
            @endrole
            @role('Admin')
            <li><a href="{{ url('/view-product') }}" class="scrollto">Beli Voucher</a></li>
            @endrole
          @auth
          <li class="drop-down"><a href="">Auth</a>
            <ul>
              @if (Route::has('login'))
                <li><a href="{{ route('login') }}">Login</a></li>
                @if (Route::has('register'))
                  <li><a href="{{ route('register') }}">Register</a></li>
                @endif
              @endif
          </li>
            @endauth
          <li><a href="{{ url('/login') }}" class="scrollto">Login</a></li>
          <li><a href="{{ url('/register') }}" class="scrollto">Register</a></li>
          
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End #header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <h1>Selamat Datang di RT RW Net</h1>
      <h2>Sedia Voucher Internet atau Wifi di sekitar wilayah RT dan RW</h2>
    </div>
  </section><!-- #hero -->

  <main id="main">
    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container">
        <h3 class="text-center"><strong>Voucher yang tersedia saat ini</strong></h3>
        <p class="text-center">
          Silakan Masuk untuk melakukan pembelian voucher internet.
        </p>
        <br>
        <div class="row">
          @if (!is_null($products))
            @foreach ($products as $product)
              <div class="col-lg-3 col-md-6 mb-3">
                <div class="card">
                  {{-- <img src="{{ asset('assets/icons-payment/voucher-wifi.jpg') }}" class="card-img-top" alt="..."> --}}
                  {{-- <div class="card-icon">
                    <i class="bx bx-book-reader"></i>
                  </div> --}}
                  <div class="card-header text-center"><strong>{{ $product->name }}</strong></div>
                  <div class="card-body">
                    {{-- <p class="card-title"><a href="">{{ $product->name }}</a></p> --}}
                    <p class="card-text text-center">@currency($product->price)</p>
                    <p class="card-text text-center">
                      @if ($product->type == 1)
                      Paket Kuota - 1 GB
                      @else
                          Unlimited
                          @endif  
                        </p>
                        {{-- <hr> --}}
                  </div>
                </div>
              </div>
            @endforeach
          @endif
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="{{ asset('assets/icons-payment/cms-package.jpg') }}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <h3><strong>Metode Pembayaran yang tersedia.</strong></h3>
            <ul>
              <li><i class="bx bx-check-double"></i> Dana.</li>
              <li><i class="bx bx-check-double"></i> Link Aja.</li>
              <li><i class="bx bx-check-double"></i> Gopay.</li>
              <li><i class="bx bx-check-double"></i> Ovo.</li>
              <li><i class="bx bx-check-double"></i> Shopeepay.</li></ul>
            <p>
              Silakan Masuk untuk melakukan pembelian voucher internet.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->


    <!-- ======= Frequenty Asked Questions Section ======= -->
    <section class="faq">
      <div class="container">

        <div class="section-title">
          <h2>Frequenty Asked Questions</h2>
        </div>

        <ul class="faq-list">

          <li>
            <a data-bs-toggle="collapse" class="collapsed" data-bs-target="#faq1">Bagaimana Cara Pesan? <i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
            <div id="faq1" class="collapse" data-bs-parent=".faq-list">
              <p>
                Silahkan lakukan proses Login jika telah memilik akun sebelumnya, dan lakukan proses Register jika belum memiliki akun. List Product akan tersedia pada menu List Product, dan pemesanan dapat dilakukan.</p>
            </div>
          </li>
          <li>
            <a data-bs-toggle="collapse" class="collapsed" data-bs-target="#faq2">Ewallet yang tersedia? <i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
            <div id="faq2" class="collapse" data-bs-parent=".faq-list">
              <p>
                Saat ini hanya tersedia Dana, dan Linkaja </div>
          </li>

        </ul>

      </div>
    </section><!-- End Frequenty Asked Questions Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>RT RW Net</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-landing-page/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End #footer -->

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>