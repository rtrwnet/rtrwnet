<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            @role('Admin')
                <div class="sb-sidenav-menu-heading">Admin</div>
                <a class="nav-link" href="{{ url('/view-product') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Produk
                </a>
                <a class="nav-link" href="{{ url('/view-history') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Transaksi
                </a>
            @endrole
            @role('User')
                <div class="sb-sidenav-menu-heading">User</div>
                <a class="nav-link" href="{{ url('/view-order') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Beli Voucher
                </a>
                <a class="nav-link" href="{{ url('/history') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Riwayat Transaksi
                </a>
            @endrole
        </div>
    </div>
    <div class="sb-sidenav-footer">
        <div class="small">Logged in as:</div>
        {{ \Auth::user()->name }}
    </div>
</nav>
