@extends('layouts.dashboard-layout')
@section('content')
    <div class="container-fluid px-4">
        <h1 class="mt-4 text-center">Beli Voucher</h1>
        <p class="text-center">Silakan pilih voucher dan selesaikan pembayaranmu!</p>
        {{-- <ol class="breadcrumb mb-4"> --}}
        {{-- </ol> --}}
        <br>
        @if (count($kuota) > 0 || count($unlimited) > 0)
            @if (session('status'))
                <div class="alert alert-success">{{ session('status') }}</div>
            @endif
            <form action="{{ url('/order') }}" method="POST">
                @csrf
                <div class="card">
                    {{-- <div class="card-header">
                Voucher
            </div> --}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    @if (count($kuota) > 0)
                                        <h5>Kuota</h5>
                                        <hr>
                                        @foreach ($kuota as $key => $kuotaData)
                                            <div class="col-lg-3">
                                                <input class="checkbox-tools" type="radio" value="{{ $kuotaData->id }}"
                                                    name="product_id" id="toolk-{{ $kuotaData->id }}"
                                                    @if ($key == 0) checked @endif>
                                                <label class="for-checkbox-tools" for="toolk-{{ $kuotaData->id }}">
                                                    <strong>{{ $kuotaData->name }}</strong> <br> @currency($kuotaData->price) -
                                                    {{ $kuotaData->bandwidth }} {{ $kuotaData->bandwidth_type }} <br>
                                                    {{ $kuotaData->active_date }}
                                                    @switch($kuotaData->active_type)
                                                        @case(1)
                                                            Hari
                                                        @break

                                                        @case(2)
                                                            Bulan
                                                        @break

                                                        @case(3)
                                                            Tahun
                                                        @break

                                                        @default
                                                    @endswitch
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                    @endif
                                    @if (count($unlimited) > 0)
                                        <h5>Unlimited</h5>
                                        <hr>
                                        @foreach ($unlimited as $key => $unlimitedData)
                                            <div class="col-lg-3">
                                                <input class="checkbox-tools" type="radio"
                                                    value="{{ $unlimitedData->id }}" name="product_id"
                                                    id="toolu-{{ $unlimitedData->id }}"
                                                    @if ($key == 0) checked @endif>
                                                <label class="for-checkbox-tools" for="toolu-{{ $unlimitedData->id }}">
                                                    <strong>{{ $unlimitedData->name }}</strong> <br> @currency($unlimitedData->price)
                                                    -
                                                    Unlimited <br>
                                                    {{ $unlimitedData->active_date }}
                                                    @switch($unlimitedData->active_type)
                                                        @case(1)
                                                            Hari
                                                        @break

                                                        @case(2)
                                                            Bulan
                                                        @break

                                                        @case(3)
                                                            Tahun
                                                        @break

                                                        @default
                                                    @endswitch
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-12">
                                {{-- <div class="row"> --}}
                                <div class="col-lg-12 p-4" style="border: 1px solid #e5e5e5;">
                                    @foreach ($paymentGroups as $paymentGroup)
                                        <h4>{{ $paymentGroup->name }}</h4>
                                        <hr>
                                        <div class="row mb-3">
                                            @foreach ($paymentGroup->payments as $key => $paymentMethod)
                                                <div class="col-lg-3">
                                                    <input class="checkbox-budget" type="radio"
                                                        value="{{ $paymentMethod->id }}" name="payment_id"
                                                        id="metode-{{ $paymentMethod->id }}"
                                                        @if ($key == 0) checked @endif>
                                                    <label class="for-checkbox-budget"
                                                        for="metode-{{ $paymentMethod->id }}">
                                                        <img src="{{ asset($paymentMethod->photo_url) }}"
                                                            style="width: 100%; height:100px" alt="" class="p-3">
                                                        {{-- <img src="{{ asset('assets/icons-payment/bca.png') }}" style="width: 100px;" alt=""> --}}
                                                    </label>
                                                </div>
                                                {{-- <div class="col-lg-4">
                                            <input class="checkbox-budget" type="radio" name="budget" id="metode-2">
                                            <label class="for-checkbox-budget" for="metode-2">							
                                                <img src="{{ asset('assets/icons-payment/bni.png') }}" style="width: 100px;" alt="">
                                            </label>
                                        </div>
                                        <div class="col-lg-4">
                                            <input class="checkbox-budget" type="radio" name="budget" id="metode-3">
                                            <label class="for-checkbox-budget" for="metode-3">							
                                                <img src="{{ asset('assets/icons-payment/bsi.png') }}" style="width: 100px;" alt="">
                                            </label>
                                        </div> --}}
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                                {{-- </div> --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mt-3">
                                    <button type="submit"
                                        class="btn btn-secondary btn-md text-white float-end mt-2 p-3"><strong><i
                                                class="fas fa-box"></i> Beli Sekarang</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @else
            <div class="card">
                <div class="card-header">
                    Voucher
                </div>
                <div class="card-body">
                    Voucher sedang habis!
                </div>
            </div>
        @endif
    </div>
    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Nama User: </label>
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Harga Transaksi: </label>
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Tipe Transaksi: </label>
                                    <select name="" id="" class="form-control">
                                        <option value="">--Pilih Tipe Transaksi--</option>
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="card-body">
                            <h4>Delete?</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
