@extends('layouts.dashboard-layout')
@section('content')
    <div class="container-fluid px-4">

        <h1 class="mt-4">Produk</h1>
        @if (session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">List Produk</li>
        </ol>
        <div class="row">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    LIST PRODUK
                    <a href="" class="btn btn-sm btn-primary ml-3 text-white float-end" data-bs-toggle="modal"
                        data-bs-target="#addModal">Tambah Produk</a>
                </div>
                <div class="card-body">
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Harga</th>
                                <th>Jenis</th>
                                {{-- <th>Bandwidth</th> --}}
                                <th>Masa Berlaku</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>@currency($product->price) </td>
                                    <td>{{ $product->type == '2' ? 'Unlimited' : 'Kuota' }}</td>
                                    {{-- <td>{{ $product->type == '2' ? 'Unlimited' : $product->bandwidth . $product->bandwidth_type }} --}}
                                    </td>
                                    <td>{{ $product->active_date }}
                                        @if ($product->active_type == 1)
                                            Hari
                                        @elseif ($product->active_type == 2)
                                            Bulan
                                        @elseif ($product->active_type == 3)
                                            Tahun
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-primary ml-3 text-white editBtn" data-bs-toggle="modal"
                                            data-bs-target="#editModal{{ $product->id }}"
                                            value="{{ $product->id }}">Edit</button>
                                        <button class="btn btn-danger ml-3 text-white deleteBtn" data-bs-toggle="modal"
                                            data-bs-target="#deleteModal" value="{{ $product->id }}">Hapus</button>
                                        <a href="{{ url('/detail-product', $product->id) }}"
                                            class="btn btn-info ml-3 text-white">Detail</a>
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="editModal{{ $product->id }}" tabindex="-1"
                                    aria-labelledby="addModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="EditModalLabel">Edit Produk</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <form action="{{ url('update-product') }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="product_id" id="product_id" value="" />
                                                <div class="modal-body">
                                                    <div class="card-body">
                                                        <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Nama Produk: </label>
                                                                <input type="text" name="name" id=""
                                                                    value="{{ $product->name }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Harga Produk: </label>
                                                                <input type="number" name="price" id=""
                                                                    value="{{ $product->price }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        {{-- <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Tipe Produk: </label>
                                                                <select name="type" id="" class="form-control">
                                                                    <option value="">Pilih Tipe Produk</option>
                                                                    <option value="1"
                                                                        {{ $product->type == 1 ? 'selected' : '' }}>Kuota
                                                                    </option>
                                                                    <option value="2"
                                                                        {{ $product->type == 2 ? 'selected' : '' }}>
                                                                        Unlimited</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Bandwidth </label>
                                                                <input type="number" name="bandwidth" id=""
                                                                    value="{{ $product->bandwidth }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Jenis Bandwidth </label>
                                                                <select name="bandwidth_type" id=""
                                                                    class="form-control">
                                                                    <option value="">Pilih Tipe Bandwidth</option>
                                                                    <option value="MB"
                                                                        {{ $product->bandwidth_type == 'MB' ? 'selected' : '' }}>
                                                                        MB</option>
                                                                    <option value="GB"
                                                                        {{ $product->bandwidth_type == 'GB' ? 'selected' : '' }}>
                                                                        GB</option>
                                                                </select>
                                                            </div>
                                                        </div> --}}
                                                        <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Masa Kadaluwarsa </label>
                                                                <input type="number" name="active_date" id=""
                                                                    value="{{ $product->active_date }}"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="mb-3">
                                                                <label for="">Jenis Masa Kadaluwarsa </label>
                                                                <select name="active_type" id=""
                                                                    class="form-control">
                                                                    <option value="">Pilih Jenis Masa Kadaluwarsa
                                                                    </option>
                                                                    <option value="1"
                                                                        {{ $product->active_type == 1 ? 'selected' : '' }}>
                                                                        Hari</option>
                                                                    <option value="2"
                                                                        {{ $product->active_type == 2 ? 'selected' : '' }}>
                                                                        Bulan</option>
                                                                    <option value="3"
                                                                        {{ $product->active_type == 3 ? 'selected' : '' }}>
                                                                        Tahun</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Edit Produk</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Tambah Produk</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ url('store-product') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Nama Produk: </label>
                                    <input type="text" name="name" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Harga Produk: </label>
                                    <input type="number" name="price" id="" class="form-control">
                                </div>
                            </div>
                            {{-- <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Tipe Produk: </label>
                                    <select name="type" id="" class="form-control">
                                        <option value="">Pilih Tipe Produk</option>
                                        <option value="0">Kuota</option>
                                        <option value="1">Unlimited</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Bandwidth </label>
                                    <input type="number" name="bandwidth" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Jenis Bandwidth </label>
                                    <select name="bandwidth_type" id="" class="form-control">
                                        <option value="">Pilih Tipe Bandwidth</option>
                                        <option value="MB">MB</option>
                                        <option value="GB">GB</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Masa Kadaluwarsa </label>
                                    <input type="number" name="active_date" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Jenis Masa Kadaluwarsa </label>
                                    <select name="active_type" id="" class="form-control">
                                        <option value="">Pilih Jenis Masa Kadaluwarsa</option>
                                        <option value="1">Hari</option>
                                        <option value="2">Bulan</option>
                                        <option value="3">Tahun</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Buat Produk</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Produk</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ url('delete-product') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <div class="card-body">
                            <h4>Delete?</h4>
                            <input type="hidden" name="delete_product_id" id="deleting_id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {

            $(document).on('click', '.deleteBtn', function() {
                var product_id = $(this).val();
                // alert(product_id);
                $('#deleteModal').modal('show');
                $('#deleting_id').val(product_id);

            })

            $(document).on('click', '.editBtn', function() {
                var product_id = $(this).val();
                $('#editModal').modal('show');

                $.ajax({
                    type: "GET",
                    url: "/edit-product/" + product_id,
                    success: function(response) {
                        // console.log(response.product.name);
                        $('#name').val(response.product.name);
                        $('#price').val(response.product.price);
                        $('#type').val(response.product.type);
                        $('#description').val(response.product.description);
                        $('#product_id').val(product_id);
                    }
                })
            });
        });
    </script>
@endsection
