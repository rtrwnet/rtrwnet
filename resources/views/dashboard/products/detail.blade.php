@extends('layouts.dashboard-layout')
@section('content')
    <div class="container-fluid px-4">
        <h1 class="mt-4">Detail Produk</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Detail Produk </li>
        </ol>
        @if (session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <p>Nama Produk : <b>{{ $product->name }}</b></p>
                    <p>Harga Produk : <b>@currency($product->price)</b></p>
                    <p>Jenis Produk : <b>{{ $product->type == '1' ? 'Unlimited' : 'Kuota' }}</b></p>
                </div>
                <div class="row">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Detail List Voucher
                            <a href="" class="btn btn-sm btn-success ml-3 text-white float-end"
                                data-bs-toggle="modal" data-bs-target="#addVoucher">Tambah Voucher</a>
                            <!-- <a href="" class="btn btn-sm btn-primary ml-3 text-white float-end"
                                data-bs-toggle="modal" data-bs-target="#addModal">Tambah Voucher</a> -->
                            <a href="" class="btn btn-sm btn-primary ml-3 mr-4 text-white float-end"
                                style="margin-right: 3px;" data-bs-toggle="modal" data-bs-target="#importModal">Import
                                Voucher</a>
                            <a href="{{ url('download') }}" class="btn btn-sm btn-info ml-3 mr-4 text-white float-end"
                                style="margin-right: 3px;">Template Excel
                                Voucher</a>
                        </div>
                        <div class="card-body">
                            <table id="datatablesSimple">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode Voucher</th>
                                        <th>Password Voucher</th>
                                        <th>Dibeli Oleh</th>
                                        <th>Dibeli Tanggal</th>
                                        {{-- <th>Tanggal Kadaluwarsa</th> --}}
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vouchers as $voucher)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $voucher->code }}</td>
                                            <td>{{ $voucher->password_voucher }}</td>
                                            <td>{{ isset($voucher->history) ? $voucher->history->user->name : '' }}</td>
                                            <td>{{ isset($voucher->history) ? $voucher->history->transaction_date : '' }}
                                            </td>
                                            {{-- <td>{{ $voucher->expired_at }}</td> --}}
                                            <td>
                                                <button class="btn btn-primary ml-3 text-white editBtn"
                                                    data-bs-toggle="modal" data-bs-target="#editModal"
                                                    value="{{ $voucher->id }}">Edit</button>
                                                <button class="btn btn-danger ml-3 text-white deleteBtn"
                                                    data-bs-toggle="modal" data-bs-target="#deleteModal"
                                                    value="{{ $voucher->id }}">Hapus</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Add Voucher</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ url('/store-voucher') }}" method="POST">
                    @csrf

                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Kode Voucher: </label>
                                    <input type="text" name="code" id="" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah Voucher</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Add Voucher -->
    <div class="modal fade" id="addVoucher" tabindex="-1" aria-labelledby="addVoucherLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Add Voucher</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ url('/create-voucher') }}" method="POST">
                @csrf

                <div class="modal-body">
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="jumlah_voucher">Jumlah Voucher: </label>
                                <input type="number" name="jumlah_voucher" id="jumlah_voucher" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="">Durasi: </label>
                                <input type="time" name="duration" id="" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah Voucher</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- Import Modal -->
    <div class="modal fade" id="importModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Import Voucher</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ url('import-voucher/' . $product->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <input type="file" name="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import Voucher</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit Produk</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ url('/update-voucher') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="voucher_id" id="voucher_id" value="" />
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Kode Voucher: </label>
                                    <input type="text" name="code" id="code" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Produk</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ url('delete-voucher') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <div class="card-body">
                            <h4>Delete?</h4>
                            <input type="hidden" name="delete_voucher_id" id="deleting_id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {

            $(document).on('click', '.deleteBtn', function() {
                var voucher_id = $(this).val();
                // alert(voucher_id);
                $('#deleteModal').modal('show');
                $('#deleting_id').val(voucher_id);

            })

            $(document).on('click', '.editBtn', function() {
                var voucher_id = $(this).val();
                $('#editModal').modal('show');

                $.ajax({
                    type: "GET",
                    url: "/edit-voucher/" + voucher_id,
                    success: function(response) {
                        // console.log(response.voucher.code);
                        $('#product_id').val(response.voucher.product_id);
                        $('#code').val(response.voucher.code);
                        $('#expired_at').val(response.voucher.expired_at);
                        $('#voucher_id').val(voucher_id);
                    }
                })
            });
        });
    </script>
@endsection
