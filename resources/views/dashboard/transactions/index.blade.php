@extends('layouts.dashboard-layout')
@section('content')
    <div class="container-fluid px-4">
        <h1 class="mt-4">Transaksi</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">List Transaksi</li>
        </ol>
        <div class="row">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Riwayat Pembelian
                </div>
                <div class="card-body">
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama User</th>
                                <th>Nama Produk</th>
                                <th>Status</th>
                                <th>Kadaluwarsa</th>
                                <th>Tanggal Transaksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($histories as $history)
                                <tr>
                                    <td>{{ $history->id }}</td>
                                    <td>{{ $history->user->name }}</td>
                                    <td>{{ $history->product->name }}</td>
                                    <td>
                                        @if ($history->transaction_status == '0')
                                            <span class="badge bg-warning">
                                                Menunggu Pembayaran
                                            </span>
                                        @elseif($history->transaction_status == '1')
                                            <span class="badge bg-success">
                                                Sukses
                                            </span>
                                        @else
                                            <span class="badge bg-danger">
                                                Gagal
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        @if (isset($history->voucher) && new \DateTime($history->voucher->expired_at) >= new \DateTime(Date('Y-m-d H:i:s')))
                                            Voucher Kadaluwarsa
                                        @else
                                            @if (isset($history->voucher) && new \DateTime($history->voucher->expired_at) < new \DateTime(Date('Y-m-d H:i:s')))
                                                Voucher aktif
                                            @endif
                                            -
                                        @endif
                                    </td>
                                    <td>{{ $history->transaction_date }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Nama User: </label>
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Harga Transaksi: </label>
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="">Tipe Transaksi: </label>
                                    <select name="" id="" class="form-control">
                                        <option value="">--Pilih Tipe Transaksi--</option>
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="card-body">
                            <h4>Delete?</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
