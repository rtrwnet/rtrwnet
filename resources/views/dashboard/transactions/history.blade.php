@extends('layouts.dashboard-layout')
@section('content')
    <div class="container-fluid px-4">

        <h1 class="mt-4">History</h1>
        @if (session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">List History</li>
        </ol>
        <div class="row">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    DataTable Example
                </div>
                <div class="card-body">
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Nama User</th>
                                <th>Nama Produk</th>
                                <th>Kode Voucher</th>
                                <th>Password Voucher</th>
                                <th>Status</th>
                                <th>Kadaluwarsa</th>
                                <th>Tanggal Transaksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($histories as $history)
                                <tr>
                                    <td>{{ $history->user->name }}</td>
                                    <td>{{ $history->product->name }}</td>
                                    <th>{{ $history->code }}</th>
                                    <th>
                                        {{ isset($history->voucher) ? $history->voucher->password_voucher : "" }}
                                    </th>
                                    <th>
                                        @if ($history->transaction_status == '0')
                                            <span class="badge bg-warning">
                                                Menunggu Pembayaran
                                            </span>
                                        @elseif($history->transaction_status == '1')
                                            <span class="badge bg-success">
                                                Sukses
                                            </span>
                                        @else
                                            <span class="badge bg-danger">
                                                Gagal
                                            </span>
                                        @endif
                                    </th>
                                    <td>
                                        @if (isset($history->voucher) && new \DateTime($history->voucher->expired_at) >= new \DateTime(Date('Y-m-d H:i:s')))
                                            Voucher Kadaluwarsa
                                        @else
                                            @if (isset($history->voucher) && new \DateTime($history->voucher->expired_at) < new \DateTime(Date('Y-m-d H:i:s')))
                                                Voucher aktif
                                            @endif
                                            -
                                        @endif
                                    </td>
                                    <td>{{ $history->transaction_date }} </td>
                                    {{-- <td>
                                     <a href="{{ url('/detail-history', $history->id) }}" class="btn btn-info ml-3 text-white">Detail</a>
                                </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
