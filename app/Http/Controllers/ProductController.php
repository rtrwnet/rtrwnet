<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\PaymentGroup;
use App\Models\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;

class ProductController extends Controller
{
    public function sellProducts()
    {
        $kuota = Product::where('type', 1)->orderBy('id', 'asc')->get();
        $unlimited = Product::where('type', 2)->orderBy('id', 'asc')->get();

        $groups = PaymentGroup::all();

        return view('dashboard.orders.index', [
            'kuota' => $kuota,
            'unlimited' => $unlimited,
            'paymentGroups' => $groups
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->get();

        return view('dashboard.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // Validate the incoming request data
            $validatedData = $request->validate([
                'name' => 'required',
                'price' => 'required|numeric',
                'active_date' => 'required|numeric',
                'active_type' => 'required',
            ]);

            $validatedData = $request->except("_method", "_token");

            // Create a new product using the validated data
            \DB::transaction(function () use ($validatedData) {
                $validatedData['type'] = 2;
                $validatedData['bandwidth'] = 0;
                $validatedData['bandwidth_type'] = "GB";
                $product = Product::create($validatedData);
            });


            return redirect()->back()->with('status', 'Berhasil menambahkan data.');
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
            return redirect()->back()->with('error', $th->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, $id)
    {
        $product = Product::findOrFail($id);
        $vouchers = Voucher::where('product_id', $id)->with(["history"])->orderBy('id', 'desc')->get();
        return view('dashboard.products.detail', compact('product', 'vouchers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return response()->json([
            'status' => 200,
            'product' => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        try {
            // Validate the incoming request data
            $validatedData = $request->validate([
                'name' => 'required',
                'price' => 'required|numeric',
                'active_date' => 'required|numeric',
                'active_type' => 'required',
            ]);


            $id = $request->product_id;
            $req = $request->except("product_id", "_method", "_token");

            // Create a new product using the validated data
            \DB::transaction(function () use ($id, $req) {
                $req['type'] = 2;
                $req['bandwidth'] = 0;
                $req['bandwidth_type'] = "GB";
                $product = Product::where('id', $id)->update($req);
            });

            return redirect()->back()->with('status', 'Berhasil mengubah data.');
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
            return redirect()->back()->with('error', $th->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        $product_id = $request->input('delete_product_id');
        $product = Product::find($product_id);
        $product->delete();

        return redirect()->back()->with('status', 'Deleted');
    }
}
