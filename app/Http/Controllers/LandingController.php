<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index()
    {
        $products = Product::limit(8)->get();
        // dd($products);
        return view('index', compact('products'));
    }
}
