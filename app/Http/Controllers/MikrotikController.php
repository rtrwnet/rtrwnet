<?php

namespace App\Http\Controllers;

use App\Models\RouterosAPI;
use App\Models\Voucher;
use Illuminate\Http\Request;
use \RouterOS\Client;
use Illuminate\Support\Str;

class MikrotikController extends Controller
{
    protected $client, $config;

    public function __construct()
    {
        // Initiate client with config object
        $this->config = config('routeros');
        $this->client = new Client([
            'host' => $this->config['host'],
            'user' => $this->config['user'],
            'pass' => $this->config['pass'],
            'port' => $this->config['port'],
            'ssl' => $this->config['ssl'],
            'socket_blocking' => false,
        ]);
    }

    public function getInterfaces()
    {
        // Send query and read response from RouterOS
        $response = $this->client->query('/interface/print')->read();

        return $response;
    }
    
    public function getHotspot()
    {
        $hostpotUser = $this->client->query('/ip/hotspot/user/print')->read();

        return $hostpotUser;
    }

    public function createVoucher(Request $request)
    {

        $durasi = $request->input('duration');
        $jmVoucher = $request->input('jumlah_voucher');

        for ($i=0; $i < $jmVoucher; $i++) { 
            $username = Str::random(6);
            $password = Str::random(8);
    
            $data = [
                'product_id' => $request->input('product_id'),
                'code' => $username,
                'password_voucher' => $password
            ];

            Voucher::create($data);
        
            $API = new RouterosAPI();
            if($API->connect($this->config['host'], $this->config['user'], $this->config['pass'])) {
                $API->comm('/ip/hotspot/user/add', [
                    'name' => $username,
                    'password' => $password, 
                    'profile' => 'default',
                    'limit-uptime' => $durasi 
                ]);
            } else {
                return dd("Koneksi Gagal!!!");
            }
    
            $API->disconnect();
        }

        return redirect()->back()->with('success', 'Voucher Berhasil Dibuat!!!');
    }
}
