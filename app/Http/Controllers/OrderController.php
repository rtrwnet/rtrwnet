<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\History;
use App\Models\Product;
use App\Models\Voucher;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\RuntimeException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use App\Jobs\GetVoucher;

class OrderController extends Controller
{
    protected $baseURL;
    protected $headers;

    public function __construct(Request $request)
    {
        $this->baseURL = config('xendit.xendit_base_url');
        // $this->baseURL = config('services.xendit.secret_key');
        $this->headers = $this->headers();
    }

    public function headers()
    {
        // $xendit_token = config('xendit.xendit_token');
        $xendit_token = config('services.xendit.secret_key');
        $base64 = base64_encode($xendit_token . ":");
        $headers = ['Authorization' => 'Basic ' . $base64];

        return $headers;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'payment_id' => 'required',
            'product_id' => 'required',
        ]);

        $payment = PaymentMethod::where('id', $request->payment_id)->first();
        $product = Product::where('id', $request->product_id)->first();
        $voucher = Voucher::where('product_id', $request->product_id)->whereNull('bought_status')->first();
        if (!$voucher) {
            return redirect()->back()->with('error', 'Voucher sudah habis');
        }

        $trxID = $this->generateTrxId();

        $history = History::create([
            'user_id' => \Auth::user()->id,
            'product_id' => $request->product_id,
            'transaction_status' => 0,
            'transaction_date' => date('Y-m-d H:i:s'),
            'transaction_id' => $trxID
        ]);

        $response = $this->payment($payment, $product, $history, $trxID);
        if ($payment->payment_code == 'SHOPEEPAY') {
            return redirect($response['channel_properties']['success_redirect_url']);
        } else {
            return redirect($response['actions']['desktop_web_checkout_url']);
        }
    }

    // public function payment($payment, $product, $history, $trxID)
    // {
    //     $redirect_url = config('app.url'). '/history-detail/'. $history->id;
    //     // dd($redirect_url);
    //     switch ($payment->payment_code) {
    //         case 'DANA':
    //             $url = $this->baseURL . '/ewallets/charges';
    //             $method = 'POST';
    //             $time = date('Y-m-d\TH:i:s', strtotime(\Carbon\Carbon::now()->addHours(1)->format('Y-m-d H:i:s')));
    //             $data = [
    //                 'reference_id' => $trxID,
    //                 'currency' => 'IDR',
    //                 'amount' => (int) 5000,
    //                 'checkout_method' => 'ONE_TIME_PAYMENT',
    //                 'channel_code' => 'ID_DANA',
    //                 'channel_properties' => [
    //                     'success_redirect_url' => $redirect_url,
    //                 ],
    //             ];

    //             return $this->sendRequest($data, $method, $url);

    //             break;
    //         case 'SHOPEEPAY':

    //             $url = $this->baseURL . '/ewallets/charges';
    //             $method = 'POST';
    //             $time = date('Y-m-d\TH:i:s', strtotime(\Carbon\Carbon::now()->addHours(1)->format('Y-m-d H:i:s')));

    //             $data = [
    //                 'reference_id' => $trxID,
    //                 'currency' => 'IDR',
    //                 'amount' => 5000,
    //                 'checkout_method' => 'ONE_TIME_PAYMENT',
    //                 'channel_code' => 'ID_SHOPEEPAY',
    //                 'channel_properties' => [
    //                     'success_redirect_url' => $redirect_url,
    //                 ],
    //                 'metadata' => [
    //                     'branch_code' => 'tree_branch'
    //                 ],
    //             ];
    //             return $this->sendRequest($data, $method, $url);
    //             break;
    //         case 'LINKAJA':
    //             $url = $this->baseURL . '/ewallets/charges';
    //             $method = 'POST';
    //             $data = [
    //                 'reference_id' => $trxID,
    //                 'currency' => 'IDR',
    //                 'amount' => (int) 5000,
    //                 'checkout_method' => 'ONE_TIME_PAYMENT',
    //                 'channel_code' => 'ID_LINKAJA',
    //                 'channel_properties' => [
    //                     'success_redirect_url' => $redirect_url,
    //                 ],
    //             ];

    //             return $this->sendRequest($data, $method, $url);
    //             break;
    //         default:
    //             return 'Error';
    //             break;
    //     }
    // }
    public function payment($payment, $product, $history, $trxID)
    {
        $redirect_url = config('app.url') . '/history-detail/' . $history->id;

        // Set kunci API Xendit
        $xenditApiKey = config('services.xendit.secret_key');
        $method = 'POST';

        // URL untuk mengirim permintaan ke API Xendit
        $url = 'https://api.xendit.co/ewallets/charges';
        
        // Data yang akan dikirimkan ke Xendit
        $data = [
            'reference_id' => $trxID,
            'currency' => 'IDR',
            'amount' => $product->price, // Ganti dengan jumlah yang sesuai
            'checkout_method' => 'ONE_TIME_PAYMENT',
            'channel_code' => 'ID_'.$payment->payment_code, // Ganti dengan kode kanal yang sesuai (misalnya: 'ID_DANA')
            'channel_properties' => [
                'success_redirect_url' => $redirect_url,
            ],
            "is_redirect_required" => true,
            'callback_url' => config('app.url') . '/xendit-callback', // Pastikan sesuai dengan yang Anda konfigurasi di dashboard Xendit
            'metadata' => [
                'branch_code' => 'tree_branch',
                'branch_area' => "ANTAPANI",
                'branch_city' => "KOTA BANDUNG"
            ],
        ];
        
        // Konfigurasi permintaan HTTP
        $client = new Client();
        $response = $client->post($url, [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($xenditApiKey . ':'),
                'Content-Type' => 'application/json',
            ],
            'json' => $data,
        ]);

        // Mengambil respons dari Xendit
        $responseData = json_decode($response->getBody(), true);

        // Handle respons sesuai kebutuhan Anda
        // Misalnya, Anda dapat mengambil URL pembayaran dari $responseData['checkout_url']
        // dan mengarahkan pengguna ke halaman pembayaran.

        return $responseData;
        // return $this->sendRequest($data, $method, $url);
    }

    public function generateTrxId()
    {
        // Get the current time in microseconds
        $timestamp = microtime(true);

        // Generate a random number
        $randomNumber = rand(1000000, 9999999);

        // Combine the timestamp and random number to create a unique transaction ID
        $trrxId = 'TRX-'.$timestamp . '-' . $randomNumber;

        // Return the transaction ID
        return str_replace('.', '-', $trrxId);
    }

    public function sendRequest($data, $method, $url)
    {
        // dd([
        //     'url' => $url,
        //     'header' => $this->headers,
        //     'data' => $data
        // ]);
        try {
            $client = new Client();
            $res = $client->request($method, $url, [
                'headers' => $this->headers,
                'json' => $data,
            ]);
            $response = json_decode($res->getBody(), true);
            return $response;
        } catch (RuntimeException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (TransferException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (ConnectException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (RequestException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (BadResponseException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (ServerException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (ClientException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        } catch (TooManyRedirectsException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        }
    }

    public function callback(Request $request)
    {
        \Log::info($request->all());
        \Log::info("====================================");
        \Log::info($request['data']);
        \Log::info("====================================");

        try {
            \DB::transaction(function () use ($request) {
                $history = History::where('transaction_id', $request['data']['reference_id'])->first();
                $voucher = Voucher::where('product_id', $history->product_id)->whereNull('bought_status')->first();
                $product = Product::where('id', $history->product_id)->first();

                History::where('transaction_id', $request['data']['reference_id'])->update([
                    'transaction_status' => 1,
                    'code' => $voucher->code
                ]);

                $expiredDate = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
                if ($product->active_type == 1) {
                    $expiredDate = \Carbon\Carbon::now()->addDays($product->active_date)->format('Y-m-d H:i:s');
                } elseif ($product->active_type == 2) {
                    $expiredDate = \Carbon\Carbon::now()->addMonths($product->active_date)->format('Y-m-d H:i:s');
                } elseif ($product->active_type == 3) {
                    $expiredDate = \Carbon\Carbon::now()->addYears($product->active_date)->format('Y-m-d H:i:s');
                }

                Voucher::where('code', $voucher->code)->update([
                    'bought_status' => 1,
                    'status' => 1,
                    'expired_at' => $expiredDate
                ]);
            });

            return 'sukses';
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
            return $th->getMessage();
        }

    }

    public function historyDetails($id)
    {
        $history = History::with('product')->where('user_id', Auth::user()->id)->where('id', $id)->first();
        return view('dashboard.transactions.history-detail', compact('history'));
    }

}
