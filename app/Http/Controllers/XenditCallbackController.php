<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\History;
use App\Models\Product;
use App\Models\Voucher;

class XenditCallbackController extends Controller
{
    public function handleCallback(Request $request)
    {
        // Dapatkan data dari pemberitahuan callback
        $data = $request->all();
        Log::channel('custom')->info($data);
        
        // Misalnya, Anda dapat memeriksa status pembayaran
        $payment = $data['data'];

        if ($payment['status'] === 'SUCCEEDED') {
            // Pembayaran berhasil, lakukan tindakan sesuai (misalnya, perbarui status di database Anda)
            Log::channel('custom')->info("PEMBAYARAN BERHASIL");
            $history = History::where('transaction_id', $payment['reference_id'])->first();
            Log::channel('custom')->info($history);
            $voucher = Voucher::where('product_id', $history->product_id)->whereNull('bought_status')->first();
            Log::channel('custom')->info($voucher);
            $product = Product::where('id', $history->product_id)->first();
            Log::channel('custom')->info($product);

            History::where('transaction_id', $request['data']['reference_id'])->update([
                'transaction_status' => 1,
                'code' => $voucher->code
            ]);

            // Disini untuk atur kode voucher kadaluarsa
            $expiredDate = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
            if ($product->active_type == 1) {
                $expiredDate = \Carbon\Carbon::now()->addDays($product->active_date)->format('Y-m-d H:i:s');
            } elseif ($product->active_type == 2) {
                $expiredDate = \Carbon\Carbon::now()->addMonths($product->active_date)->format('Y-m-d H:i:s');
            } elseif ($product->active_type == 3) {
                $expiredDate = \Carbon\Carbon::now()->addYears($product->active_date)->format('Y-m-d H:i:s');
            }

            Voucher::where('code', $voucher->code)->update([
                'bought_status' => 1,
                'status' => 1,
                'expired_at' => $expiredDate
            ]);

        } else {
            // Pembayaran tidak berhasil, lakukan tindakan sesuai (misalnya, memberikan pesan kesalahan)
            Log::channel('custom')->info("PEMBAYARAN TIDAK BERHASIL");
        }

        // Lakukan pemrosesan sesuai dengan data yang diterima dari Xendit
        // Misalnya, perbarui status pembayaran di database Anda berdasarkan data ini.

        // Kemudian, Anda dapat memberikan respons yang sesuai ke Xendit, seperti "OK" (200 OK).

        return response('OK', 200);
    }
}
