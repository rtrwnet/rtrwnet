<?php

namespace App\Http\Controllers;

use App\Models\Voucher;
use Illuminate\Http\Request;
use App\Imports\VoucherImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Response;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'product_id' => 'required',
            'code' => 'required',
        ]);

        // Create a new product using the validated data
        $voucher = Voucher::create($validatedData);

        return redirect()->back()->with('status', 'Sukses');
    }

    public function import(Request $request, $id)
    {
        $file = $request->file('file');

        Excel::import(new VoucherImport($id), $file);

        return redirect()->back()->with('success', 'Import completed!');
    }

    public function downloadFile()
    {
        $filePath = public_path('template.xlsx');

        return Response::download($filePath, 'template.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher, $id)
    {
        $voucher = Voucher::find($id);

        return response()->json([
            'status' => 200,
            'voucher' => $voucher,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        $voucher_id = $request->input('voucher_id');
        $voucher = Voucher::find($voucher_id)->update([
            'product_id' => $request->input('product_id'),
            'code' => $request->input('code'),
        ]);

        return redirect()->back()->with('status', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $voucher_id = $request->input('delete_voucher_id');
        $voucher = Voucher::find($voucher_id);
        $voucher->delete();

        return redirect()->back()->with('status', 'Deleted');
    }
}
