<?php

namespace App\Imports;

use App\Models\Voucher;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VoucherImport implements ToModel, WithHeadingRow
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }


    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Voucher([
            'product_id' => $this->id,
            'code' => $row['voucher_code'],
        ]);
    }
}
