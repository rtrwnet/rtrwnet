<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class History extends Model
{
    use HasFactory;
    protected $table = 'history';

    protected $fillable = [
        'user_id',
        'product_id',
        'transaction_status',
        'transaction_date',
        'transaction_id',
        'code'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'code', 'code');
    }
}
