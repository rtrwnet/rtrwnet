<?php

namespace App\Models;

use App\Models\Product;
use App\Models\History;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Voucher extends Model
{
    use HasFactory;
    protected $table = 'voucher';

    protected $fillable = [
        'product_id',
        'code',
        'password_voucher',
        'expired_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function history()
    {
        return $this->hasOne(History::class, 'code', 'code');
    }
}
