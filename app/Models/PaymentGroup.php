<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PaymentMethod;

class PaymentGroup extends Model
{
    protected $table = 'payment_group';

    protected $fillable = [
        'name',
    ];

    public function payments()
    {
        return $this->hasMany(PaymentMethod::class, 'group_id');
    }
}
