<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = 'payment_method';

    protected $fillable = [
        'name',
        'payment_code',
        'group_id',
        'photo_url',
    ];

    public function group()
    {
        return $this->belongsTo(PaymentGroup::class);
    }
}
