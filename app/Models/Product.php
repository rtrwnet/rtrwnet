<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';

    protected $fillable = [
        'name',
        'price',
        'type',
        'bandwidth',
        'bandwidth_type',
        'active_date',
        'active_type'
    ];

    public function vouchers()
    {
        return $this->hasMany(Voucher::class, 'voucher_id', 'id');
    }


}
