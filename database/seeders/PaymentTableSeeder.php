<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentGroup;
use App\Models\PaymentMethod;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a payment group
        $paymentGroup = PaymentGroup::create([
            'name' => 'E-Wallets',
        ]);

        // Create a payment method
        $paymentMethod = PaymentMethod::create([
            'name' => 'Dana',
            'payment_code' => 'DANA',
            'photo_url' => 'assets/icons-payment/dana.png',
            'group_id' => $paymentGroup->id,
        ]);

        // Create another payment method
        $paymentMethod = PaymentMethod::create([
            'name' => 'LINKAJA',
            'payment_code' => 'LINKAJA',
            'photo_url' => 'assets/icons-payment/linkaja1.png',
            'group_id' => $paymentGroup->id,
        ]);

        // Create another payment method
        $paymentMethod = PaymentMethod::create([
            'name' => 'Shopeepay',
            'payment_code' => 'SHOPEEPAY',
            'photo_url' => 'assets/icons-payment/shopee.png',
            'group_id' => $paymentGroup->id,
        ]);
    }
}
