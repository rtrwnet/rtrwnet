<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::query()->delete();
        Role::query()->delete();
        \DB::table('model_has_roles')->delete();

        User::insert([
            ['name' => 'Admin', 'email' => 'admin@test.id', 'email_verified_at' => '2020-01-28 09:36:03', 'password' => bcrypt('admin2023'), 'remember_token' => Str::random(60)],
            ['name' => 'Pengguna 1', 'email' => 'user@test.id', 'email_verified_at' => '2020-01-28 09:36:03', 'password' => bcrypt('user2023'), 'remember_token' => Str::random(60)],
        ]);

        Role::insert([
            ['name' => 'Admin', 'guard_name' => 'Admin'],
            ['name' => 'User', 'guard_name' => 'User']
        ]);

        \DB::table('model_has_roles')->insert([
            [
                'role_id'=> 1,
                'model_type'=> 'App\\Models\\User',
                'model_id'=>1
            ],
            [
                'role_id'=> 2,
                'model_type'=> 'App\\Models\\User',
                'model_id'=>2
            ],
        ]);
    }
}
