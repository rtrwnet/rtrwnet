<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Voucher;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // // Create 10 products
        // for ($i = 1; $i <= 10; $i++) {
        //     $product = Product::create([
        //         'name' => "Product $i",
        //         'price' => 5000 * $i,
        //         'type' => 1,
        //         'bandwidth' => $i,
        //         'bandwidth_type' => "GB",
        //         'active_date' => 1,
        //         'active_type' => 2
        //     ]);

        //     // Create a voucher for each product
        //     for ($j = 1; $j <= 5; $j++) {
        //         Voucher::create([
        //             'product_id' => $product->id,
        //             'code' => "CODE-$i-$j",
        //             // 'expired_at' => date('Y-m-d H:i:s', strtotime('+1 month')),
        //         ]);
        //     }
        // }


        for ($i = 1; $i <= 10; $i++) {
            $product = Product::create([
                'name' => "Product $i",
                'price' => 5000 * $i,
                'type' => 2,
                'bandwidth' => 0,
                'bandwidth_type' => "UNLIMITED",
                'active_date' => 1,
                'active_type' => 2
            ]);

            // Create a voucher for each product
            for ($j = 1; $j <= 5; $j++) {
                Voucher::create([
                    'product_id' => $product->id,
                    'code' => "CODE-$i-$j",
                    'expired_at' => date('Y-m-d H:i:s', strtotime('+1 month')),
                ]);
            }
        }

    }
}
