<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('voucher', 'password_voucher')) {
            Schema::table('voucher', function (Blueprint $table) {
                $table->string('password_voucher')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('voucher', 'password_voucher')) {
            Schema::table('voucher', function (Blueprint $table) {
                $table->dropColumn('password_voucher');
            });
        }
    }
}
