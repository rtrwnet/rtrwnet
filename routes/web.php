<?php

use App\Http\Controllers\LandingController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::controller(App\Http\Controllers\LandingController::class)->group(function () {
    Route::get('/', 'index');
});

Auth::routes();

Route::controller(App\Http\Controllers\MikrotikController::class)->middleware(['auth', 'role:Admin'])->group(function() {
    Route::get('/testing', 'getInterfaces');
    Route::get('/hotspot-check', 'getHotspot');
    Route::post('/create-voucher', 'createVoucher');
});

Route::controller(App\Http\Controllers\ProductController::class)->middleware(['auth', 'role:Admin'])->group(function () {
    Route::get('/view-product', 'index');
    Route::post('/store-product', 'store');
    Route::get('/edit-product/{id}', 'edit');
    Route::put('/update-product', 'update');
    Route::delete('/delete-product', 'destroy');
    Route::get('/detail-product/{id}', 'show');
});

Route::controller(App\Http\Controllers\VoucherController::class)->middleware(['auth', 'role:Admin'])->group(function () {
    Route::post('store-voucher', 'store');
    Route::get('edit-voucher/{id}', 'edit');
    Route::put('update-voucher', 'update');
    Route::delete('delete-voucher', 'destroy');
    Route::post('import-voucher/{id}', 'import')->name('import.store');
    Route::get('download', 'downloadFile');
});

Route::controller(App\Http\Controllers\HistoryController::class)->middleware(['auth', 'role:Admin'])->group(function () {
    Route::get('/view-history', 'index');
    Route::get('/detail-history/{id}', 'show');
});
Route::controller(App\Http\Controllers\HistoryController::class)->middleware(['auth', 'role:User'])->group(function () {
    Route::get('/history', 'history');

});


Route::controller(App\Http\Controllers\ProductController::class)->middleware(['auth', 'role:User'])->group(function () {
    Route::get('/view-order', 'sellProducts');
});

Route::controller(App\Http\Controllers\OrderController::class)->middleware(['auth', 'role:User'])->group(function () {
    Route::get('/history-detail/{id}', 'historyDetails');
    Route::post('/order', 'store');
});


Route::get('/redirect', function () {
    if (\Auth::user()->getRoleNames()->first() == 'Admin') {
        return redirect('/view-product');
    } else {
        return redirect('/view-order');
    }
})->middleware(['auth']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/xendit-callback', [App\Http\Controllers\XenditCallbackController::class, 'handleCallback'])->name('xendit-callback');