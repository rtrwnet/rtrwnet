<?php

return [
    'host' => env('HOST_MIKROTIK', '192.168.20.1'),
    'user' => env('USER_MIKROTIK', 'admin'),
    'pass' => env('PASS_MIKROTIK', 'YOUR_PASS'),
    'port' => intval(env('PORT_MIKROTIK', 8728)),
    'ssl' => false,
];